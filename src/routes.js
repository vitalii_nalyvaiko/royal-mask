import React from 'react';
import { Route, IndexRoute } from 'react-router';

import App from './components/App';
import Succsess from './components/success/Succsess';

export default (
    <Route path="/">
        <IndexRoute component={App}/>
        <Route path="/success" component={Succsess}/>
    </Route>
);
