import * as types from './actionTypes';


export const switchLanguage = language => ({
    type: types.SWITCH_LANGUAGE,
    language
});
