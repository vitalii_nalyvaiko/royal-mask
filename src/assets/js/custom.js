/*  4
		6.03.17
	Yaroslav Romanenko
	yarikrom05@gmail.com
*/
$(document).ready(function(){

		var sliderRev = $(".sxs-reviev"),
		sliderArea = $(".sxs-reviews"),
		indexRev = 1,
		indexmax = sliderRev.length,
		controlsBtn = "",
		controlsArea="<div class='owl2-controls'>",
		intervalID = 0,
		intervalTime = 12000;
		for (var i = 0; i <= indexmax-1; i++) {
			controlsBtn+="<div class='owl2-dot' id='ddot"+i+"'><span></span></div>";
		}
        controlsArea+=controlsBtn+"</div>";
		sliderArea.append(controlsArea);
		var dots = 	$(".owl2-dot");
		function autoChange(){
			indexRev++;
			var dotReIndex =indexRev-1;
			if(indexRev > indexmax){
				dotReIndex = indexRev-1;
					indexRev=1;

			}
        	sliderRev.fadeOut(500).removeClass("active");
        		dots.removeClass("active");
			sliderRev.filter(":nth-child("+indexRev+")").fadeIn(500).addClass("active");
		    dots.filter(":nth-child("+indexRev+")").addClass("active");
		}
      
       intervalID = setInterval(autoChange, intervalTime);
       if($(window).width()>990){
      clearInterval(intervalID);
       }


		$(window).resize(function(){
			if($(window).width()<990){
				 clearInterval(intervalID);
				intervalID = setInterval(autoChange, intervalTime);
      
			}
			else{
				sliderRev.fadeIn(500);
				 clearInterval(intervalID);
			}
		});


if($(window).width()<=990){

		sliderArea.hover(function(){
         clearInterval(intervalID);
		});
        sliderArea.mouseleave(function(){
         clearInterval(intervalID);
				intervalID = setInterval(autoChange, intervalTime);
		});
	}


		



		$(".owl2-dot").click(function(){
			var idDot= $(this).attr("id"),

			indexRev =parseInt(idDot[idDot.length-1]);

        	sliderRev.fadeOut(500);
        	dots.removeClass("active");
			sliderRev.filter(":nth-child("+(indexRev+1)+")").fadeIn(500);
		    dots.filter(":nth-child("+(indexRev+1)+")").addClass("active");
		});
		$("#orderbtn1 , #orderbtn2").click(function(event){
			event.preventDefault();
			var id  = $(this).attr('href'),
			top = $(id).offset().top;
			$('html,body').animate({'scrollTop': top}, 1500);
		});
 });

