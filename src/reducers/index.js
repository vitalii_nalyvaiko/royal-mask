import { combineReducers } from 'redux';

import content from './languageReducer';


const rootReducer = combineReducers({
    content,
});

export default rootReducer;
