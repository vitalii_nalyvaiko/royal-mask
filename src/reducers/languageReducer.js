import initialState from './initialState';
import * as types from '../actions/actionTypes';
import { getContent } from '../api';


export default (state = initialState.content, action) => {
    switch (action.type) {
        case types.SWITCH_LANGUAGE:
            return getContent(action.language);
        default:
            return state;
    }
};
