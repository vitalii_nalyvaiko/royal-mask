import React from 'react';
import PropTypes from 'prop-types';
// import { Link } from 'react-router';

import SvsIcon2 from "../../assets/images/svs-icon2.png";
import SvsIcon1 from "../../assets/images/svs-icon1.png";
import SvsProduct from "../../assets/images/svs-product.png";


const Footer = ({
    price,
    title,
    svsActions,
    form
}) => (
    <footer>
        <div className="container">

            <div className="col-md-5">
                <div className="awards">
                    <div className="award"><img src={SvsIcon2} alt=""/>
                    </div>
                    <div className="award"><img src={SvsIcon1} alt=""/>
                    </div>
                </div>
            </div>
            <div className="col-md-7">
                <h2 className="section-title">{title}</h2>
                <div className="col-md-6">
                <div className="row">
                    <div className="svs-product"><img src={SvsProduct} alt=""/>
                    </div>
                </div>
                </div>
                <div className="col-md-6">
                    <div className="svs-form">
                        <div style={{display: "none"}} className="svs-actions">
                            <div className="fs-actions-text">{svsActions.text}</div>
                            <div className="fs-numbers-group" id="fs-num-group">
                                <div className="fs-numbers"><p>0</p></div>
                                <div className="fs-numbers active"><p>1</p></div>
                                <div className="fs-numbers active"><p>8</p></div>
                            </div>
                        </div>
                        <div className="svs-actions-on">
                            <div className="new-price">{svsActions.newPrice}<span className="al-cost">{price}</span>
                            </div>
                        </div>
                        <form action="/land/order" method="POST" autoComplete="off" name="order" id="form1" className="al-form">
                            <select className="al-country" name="country" style={{display: "none"}}/>
                            <div className="form-group">
                                <label htmlFor="u_name" className="inp_label">{form.name}</label>
                                <input id="u_name" type="name" className="form-control" name="name"/>
                            </div>
                            <div className="form-group">
                                <label htmlFor="u_tel" className="inp_label">{form.tel}</label>
                                <input id="u_tel" type="tel" className="form-control" name="phone"/>
                            </div>
                            <div className="order-button">
                                <input type="submit" value={form.orderBtn}/>
                            </div>
                            <div className="security "><span>{form.security}</span></div>
                        </form>
                    </div>
                </div>
            </div>

        </div>
    </footer>
);

Footer.propTypes = {
    price: PropTypes.string.isRequired,
    title: PropTypes.string.isRequired,
    svsActions: PropTypes.object.isRequired,
    form: PropTypes.object.isRequired
};

export default Footer;
