import React from 'react';
import PropTypes from 'prop-types';

import Comp1 from "../../assets/images/comp1.png";
import Comp2 from "../../assets/images/comp2.png";
import Comp3 from "../../assets/images/comp3.png";
import TsProduct from "../../assets/images/ts-product.png";
import Comp4 from "../../assets/images/comp4.png";
import Comp5 from "../../assets/images/comp5.png";
import Comp6 from "../../assets/images/comp6.png";


const Section3 = ({
    title,
    tsComponent,
    orderBtn
}) => (
    <section id="section3">
        <div className="container">
            <div className="row">
                <h2 className="section-title">{title}</h2>
            </div>
            <div className="row">
                <div className="ts-components">
                    <div className="col-md-3 c-left">
                        <div className="ts-component">
                            <div className="component-img com1"><img src={Comp1} alt=""/></div>
                            <div className="component-title">{tsComponent.title.first}</div>
                            <div className="ts-component-text"><p>{tsComponent.text.first}</p></div>
                        </div>
                        <div className="ts-component">
                        <div className="component-img com2"><img src={Comp2} alt=""/></div>
                            <div className="component-title">{tsComponent.title.second}</div>
                            <div className="ts-component-text "><p>{tsComponent.text.second}</p></div>
                        </div>
                        <div className="ts-component">
                        <div className="component-img com3"><img src={Comp3} alt=""/></div>
                            <div className="component-title">{tsComponent.title.third}</div>
                            <div className="ts-component-text "><p>{tsComponent.text.third}</p></div>
                        </div>
                    </div>
                    <div className="col-md-6">
                        <div className="ts-comp-prod">
                            <div className="ts-component-img"><img src={TsProduct} alt=""/></div>
                        </div>
                    </div>
                    <div className="col-md-3 c-right">
                        <div className="ts-component">
                        <div className="component-img com4"><img src={Comp4} alt=""/></div>
                            <div className="component-title">{tsComponent.title.fourth}</div>
                            <div className="ts-component-text "><p>{tsComponent.text.fourth}</p></div>
                        </div>
                        <div className="ts-component">
                        <div className="component-img com5"><img src={Comp5} alt=""/></div>
                            <div className="component-title">{tsComponent.title.fifth}</div>
                            <div className="ts-component-text"><p>{tsComponent.text.fifth}</p></div>
                        </div>
                        <div className="ts-component">
                        <div className="component-img com6"><img src={Comp6} alt=""/></div>
                            <div className="component-title">{tsComponent.title.sixth}</div>
                            <div className="ts-component-text "><p>{tsComponent.text.sixth}</p></div>
                        </div>
                    </div>
                </div>
            </div>
<div className="row"> <div className="cta-button"><a href="#form1" id="orderbtn2">{orderBtn}</a>
                            </div></div>
        </div>
    </section>
);

Section3.propTypes = {
    title: PropTypes.string.isRequired,
    tsComponent: PropTypes.object.isRequired,
    orderBtn: PropTypes.string.isRequired
};

export default Section3;
