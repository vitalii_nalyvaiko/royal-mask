import React from 'react';
import PropTypes from 'prop-types';

import Select from 'react-select';
import 'react-select/dist/react-select.css';

import FsProduct from "../../assets/images/fs-product.png";

import './style.scss';

const options = [
    { value: 'cz', label: 'cz' },
    { value: 'fr', label: 'fr' },
    { value: 'gr', label: 'gr' },
    { value: 'hu', label: 'hu' },
    { value: 'it', label: 'it' },
    { value: 'pl', label: 'pl' },
    { value: 'pt', label: 'pt' },
    { value: 'ro', label: 'ro' },
    { value: 'sk', label: 'sk' },
    { value: 'es', label: 'es' }
];

const Header = ({
    price,
    productName,
    benefitTitle,
    prodBenefits,
    orderBtn,
    language,
    switchLanguage
}) => (
    <header>
        <Select style={{width: "50px"}}
            name="language"
            value={language}
            options={options}
            clearable={false}
            onChange={switchLanguage}
            className="lang--selector"/>
        <div className="container">
            <div className="col-md-offset-6 col-md-6">
                <div className="fs-block">
                    <div className="row">
                        <div className="col-md-6 col-lg-6">
                            <div className="fs-product-name">
                                <h5>{productName}</h5>
                            </div>
                        </div>
                        <div className="col-md-6"/>
                    </div>
                    <div className="row">
                        <div className="fs-benefit-title">
                            <h1>{benefitTitle}</h1>
                        </div>
                    </div>
                    <div className="fs-benefits">
                        <div className="row">
                            <div className="col-md-4 col-xs-4">
                                <div className="fs-prod">
                                    <div className="fs-prod-img"><img src={FsProduct} alt=""/>
                                    </div>
                                    <div className="fs-prod-price"><span className="al-cost">{price}</span></div>
                                </div>
                            </div>

                            <div className="col-md-8 col-xs-8">
                                <div className="fs-prod-benefits">
                                    <ul>
                                        <li>{prodBenefits.first}</li>
                                        <li>{prodBenefits.second}</li>
                                        <li>{prodBenefits.third}</li>
                                        <li>{prodBenefits.fourth}</li>
                                    </ul>
                                </div>
                                <div className="cta-button">
                                    <a href="#form1" id="orderbtn1">{orderBtn}</a>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </header>
);

Header.propTypes = {
    price: PropTypes.string.isRequired,
    productName: PropTypes.string.isRequired,
    benefitTitle: PropTypes.string.isRequired,
    prodBenefits: PropTypes.object.isRequired,
    orderBtn: PropTypes.string.isRequired,
    language: PropTypes.string.isRequired,
    switchLanguage: PropTypes.func.isRequired,
};

export default Header;
