import React from 'react';
import PropTypes from 'prop-types';

import FisIcons from "../../assets/images/fis-icons.png";
import FisRightImg from "../../assets/images/fis-right-img.png";

const Section5 = ({
    title,
    pFirst,
    pSecond
}) => (
    <section id="section5">
        <div className="container">
            <div className="row">
                <div className="col-md-12">
                <h2 className="section-title">{title}</h2>
                </div>
            </div>
            <div className="row">
                <div className="col-md-6">
                    <div className="fis-text">
                        <p>{pFirst}</p>
                        <p>{pSecond}</p>
                        <div className="icons"><img src={FisIcons} alt=""/></div>
                    </div>
                </div>
                <div className="col-md-6">
                    <div className="fis-right-img"><img src={FisRightImg} alt=""/></div>
                </div>

            </div>
        </div>
    </section>
);

Section5.propTypes = {
    title: PropTypes.string.isRequired,
    pFirst: PropTypes.string.isRequired,
    pSecond: PropTypes.string.isRequired
};

export default Section5;
