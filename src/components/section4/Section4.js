import React from 'react';
import PropTypes from 'prop-types';

import FsRound1 from "../../assets/images/fs-round-1.png";
import FsRound2 from "../../assets/images/fs-round-2.png";
import FsRound3 from "../../assets/images/fs-round-3.png";


const Section4 = ({
    title,
    subTitle,
    effectText
}) => (
    <section id="section4">
        <div className="container">
            <div className="row">
                <h2 className="section-title">{title}</h2>
                <h3 className="section-sub-title">{subTitle}</h3>
            </div>
            <div className="row">
                <div className="fs-how-iu">
                    <div className="col-md-4 item-left ">
                    <div className="fs-effect-out">
                        <div className="fs-hiu-effect">
                            <div className="fs-efect-numb">1</div>
                            <div className="fs-round-img"><img src={FsRound1} alt=""/></div>
                            <div className="fs-hiu-effect-text">{effectText.first}</div>
                        </div>
                        </div>
                    </div>
                    <div className="col-md-4 item-center">
                     <div className="fs-effect-out">
                        <div className="fs-hiu-effect">
                            <div className="fs-efect-numb">2</div>
                            <div className="fs-round-img"><img src={FsRound2} alt=""/></div>
                            <div className="fs-hiu-effect-text">{effectText.second}</div>
                        </div>
                        </div>
                    </div>
                    <div className="col-md-4 item-righgt">
                     <div className="fs-effect-out">
                        <div className="fs-hiu-effect">
                            <div className="fs-efect-numb">3</div>
                            <div className="fs-round-img"><img src={FsRound3} alt=""/></div>
                            <div className="fs-hiu-effect-text">{effectText.third}</div>
                        </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
);

Section4.propTypes = {
    title: PropTypes.string.isRequired,
    subTitle: PropTypes.string.isRequired,
    effectText: PropTypes.object.isRequired
};

export default Section4;
