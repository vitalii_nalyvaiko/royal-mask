import React from 'react';
import PropTypes from 'prop-types';

import SxsReviev1 from "../../assets/images/sxs-reviev1.png";
import SxsReviev2 from "../../assets/images/sxs-reviev2.png";
import SxsReviev3 from "../../assets/images/sxs-reviev3.png";


const Section6 = ({
    title,
    review
}) => (
    <section id="section6">
        <div className="container">
            <div className="row">
                <h2 className="section-title">{title}</h2>
            </div>
            <div className="sxs-reviews item-slider row">
                <div className="sxs-reviev">
                    <div className="sxs-buyer-name">{review.name.first}</div>
                    <div className="sxs-reviev-time">Sat, 10:30 AM</div>
                    <div className="sxs-reviev-text">
                        <p>{review.text.first}</p>
                    </div>
                    <div className="sxs-buyer-photo"><img src={SxsReviev1} alt=""/>
                    </div>
                </div>
                <div className="sxs-reviev">
                    <div className="sxs-buyer-name">{review.name.second}</div>
                    <div className="sxs-reviev-time">Sat, 09:45 AM</div>
                    <div className="sxs-reviev-text">
                        <p>{review.text.second}</p>
                    </div>
                    <div className="sxs-buyer-photo"><img src={SxsReviev2} alt=""/>
                    </div>
                </div>
                <div className="sxs-reviev">
                    <div className="sxs-buyer-name">{review.name.third}</div>
                    <div className="sxs-reviev-time">Sat, 12:20 AM</div>
                    <div className="sxs-reviev-text">
                        <p>{review.text.third}</p>
                    </div>
                    <div className="sxs-buyer-photo"><img src={SxsReviev3} alt=""/>
                    </div>
                </div>
            </div>
        </div>
    </section>
);

Section6.propTypes = {
    title: PropTypes.string.isRequired,
    review: PropTypes.object.isRequired
};

export default Section6;
