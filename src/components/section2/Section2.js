import React from 'react';
import PropTypes from 'prop-types';


import SsRound1 from "../../assets/images/ss-round-1.png";
import SsRound2 from "../../assets/images/ss-round-2.png";
import SsRound3 from "../../assets/images/ss-round-3.png";

const Section2 = ({
    title,
    achievement
}) => (
    <section id="section2">
        <div className="container">
            <div className="row">
                <h2 className="section-title">{title}</h2>
            </div>
            <div className="row">
                <div className="col-md-4 item-left">
                    <div className="ss-achievement">
                        <div className="ss-achievement-inner">
                            <div className="ss-round-img"><img src={SsRound1} alt=""/></div>
                            <div className="ss-achievement-text">{achievement.first}</div>
                        </div>
                    </div>
                </div>
                <div className="col-md-4 item-center">
                    <div className="ss-achievement">
                        <div className="ss-achievement-inner">
                            <div className="ss-round-img"><img src={SsRound2} alt=""/></div>
                            <div className="ss-achievement-text">{achievement.second}</div>
                        </div>
                    </div>
                </div>
                <div className="col-md-4 item-righgt">
                    <div className="ss-achievement">
                        <div className="ss-achievement-inner">
                            <div className="ss-round-img"><img src={SsRound3} alt=""/></div>
                            <div className="ss-achievement-text">{achievement.third}</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
);

Section2.propTypes = {
    title: PropTypes.string.isRequired,
    achievement: PropTypes.object.isRequired
};

export default Section2;
