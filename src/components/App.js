import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';


import Header from './header/Header';
import Section2 from './section2/Section2';
import Section3 from './section3/Section3';
import Section4 from './section4/Section4';
import Section5 from './section5/Section5';
import Section6 from './section6/Section6';
import Footer from './footer/Footer';

import * as languageActions from '../actions/languageActions';

import "../assets/css/style.css";
import "../assets/css/responsive.css";
import "./style.scss";

class App extends Component {
    constructor(props) {
        super(props);

        this.switchLanguage = this.switchLanguage.bind(this);
        this.setFormAction = this.setFormAction.bind(this);
    }

    componentDidMount() {
        var sliderRev = $(".sxs-reviev"),
		sliderArea = $(".sxs-reviews"),
		indexRev = 1,
		indexmax = sliderRev.length,
		controlsBtn = "",
		controlsArea="<div class='owl2-controls'>",
		intervalID = 0,
		intervalTime = 12000;
		for (var i = 0; i <= indexmax-1; i++) {
			controlsBtn+="<div class='owl2-dot' id='ddot"+i+"'><span></span></div>";
		}
        controlsArea+=controlsBtn+"</div>";
		sliderArea.append(controlsArea);
		var dots = 	$(".owl2-dot");
		function autoChange(){
			indexRev++;
            var dotReIndex = indexRev-1;
			if(indexRev > indexmax){
				dotReIndex = indexRev-1;
					indexRev = 1;

			}
            sliderRev.fadeOut(500).removeClass("active");
            dots.removeClass("active");
            sliderRev.filter(":nth-child("+indexRev+")").fadeIn(500).addClass("active");
            dots.filter(":nth-child("+indexRev+")").addClass("active");
		}

        intervalID = setInterval(autoChange, intervalTime);
        if($(window).width()>990){
            clearInterval(intervalID);
        }


		$(window).resize(function(){
			if ($(window).width()<990) {
				clearInterval(intervalID);
				intervalID = setInterval(autoChange, intervalTime);
			} else {
				sliderRev.fadeIn(500);
                clearInterval(intervalID);
            }
		});

        if ($(window).width()<=990) {
            sliderArea.hover(function(){
                clearInterval(intervalID);
            });
            sliderArea.mouseleave(function(){
                clearInterval(intervalID);
                intervalID = setInterval(autoChange, intervalTime);
            });
        }

		$(".owl2-dot").click(function(){
			var idDot = $(this).attr("id"),

			indexRev = parseInt(idDot[idDot.length-1]);

            sliderRev.fadeOut(500);
            dots.removeClass("active");
			sliderRev.filter(":nth-child("+(indexRev+1)+")").fadeIn(500);
            dots.filter(":nth-child("+(indexRev+1)+")").addClass("active");
		});
		$("#orderbtn1 , #orderbtn2").click(function(event){
			event.preventDefault();
			var id  = $(this).attr('href'),
			top = $(id).offset().top;
			$('html,body').animate({'scrollTop': top}, 1500);
		});


        this.setFormAction(this.props.content.lang);
    }

    componentDidUpdate() {
        this.setFormAction(this.props.content.lang);
    }

    switchLanguage(e) {
        this.props.changeContent(e.value);
    }

    setFormAction(language) {
        let queryStr = window.location.search;
        let currentRequestModify = window.location.pathname+'api-'+language+'.php';

        let form1 = document.getElementById('form1');
        form1.setAttribute("action", currentRequestModify+queryStr);
        form1.setAttribute("method", "post");
        form1.querySelector('input[name=phone]').required = true;
        form1.querySelector('input[name=name]').required = true;
    }

    render() {
        const { content } = this.props;

        return (
            <div className="wrapper" id="wrapper">
                <Header
                    price={content.pages.main.price}
                    productName={content.pages.main.header.productName}
                    actionsText={content.pages.main.header.actionsText}
                    benefitTitle={content.pages.main.header.benefitTitle}
                    prodBenefits={content.pages.main.header.prodBenefits}
                    orderBtn={content.pages.main.header.orderBtn}
                    language={content.lang}
                    switchLanguage={this.switchLanguage}/>
                <Section2
                    title={content.pages.main.section2.title}
                    achievement={content.pages.main.section2.achievement}/>
                <Section3
                    title={content.pages.main.section3.title}
                    tsComponent={content.pages.main.section3.tsComponent}
                    orderBtn={content.pages.main.section3.orderBtn}/>
                <Section4
                    title={content.pages.main.section4.title}
                    subTitle={content.pages.main.section4.subTitle}
                    effectText={content.pages.main.section4.effectText}/>
                <Section5
                    title={content.pages.main.section5.title}
                    pFirst={content.pages.main.section5.pFirst}
                    pSecond={content.pages.main.section5.pSecond}/>
                <Section6
                    title={content.pages.main.section6.title}
                    review={content.pages.main.section6.review}/>
                <Footer
                    price={content.pages.main.price}
                    title={content.pages.main.footer.title}
                    svsActions={content.pages.main.footer.svsActions}
                    form={content.pages.main.footer.form}/>
            </div>
        );
    }
}

App.propTypes = {
    content: PropTypes.object.isRequired,
    changeContent: PropTypes.func.isRequired
};

const mapStateToProps = state => ({
    content: state.content
});

const mapDispatchToProps = dispatch => ({
    changeContent: bindActionCreators(languageActions.switchLanguage, dispatch)
});

export default connect(mapStateToProps, mapDispatchToProps)(App);
