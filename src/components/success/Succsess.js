import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Link } from 'react-router';


import './Success.scss';

const Success = ({
    content
}) => (
    <div className="success--page">
      <div className="success--message">
        <h2>{content.pages.success.title.first}</h2>
        <h2>{content.pages.success.title.second}</h2>
        <h3>{content.pages.success.title.third}</h3>
        <Link to="/" className="success--btn-back">{content.pages.success.returnBtn}</Link>
      </div>
    </div>
);

Success.propTypes = {
    content: PropTypes.object.isRequired
};

const mapStateToProps = state => ({
    content: state.content
});

export default connect(mapStateToProps)(Success);
