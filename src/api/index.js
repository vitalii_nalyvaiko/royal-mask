import content from '../data/content';


export function getContent(language) {
    if (!language) {
        language = navigator.languages && navigator.languages[0] ||
            navigator.language || navigator.userLanguage;
    }

    if (language) {
        if (language.length > 2) {
            language = language.slice(0, 2);
        }
    } else {
        language = 'it'
    }

    const langContent = content.filter(obj => obj.lang === language)[0];

    return langContent ? langContent : content.filter(obj => obj.lang === 'it')[0];
}
